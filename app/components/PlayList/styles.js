export default {
  default: {
    position: 'relative',
    height: '100%',
    overflow: 'auto',
  },
  hidden: {
    width: 25,
    backgroundColor: 'transparent'
  }
}

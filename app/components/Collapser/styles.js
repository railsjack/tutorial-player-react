export default {
  default: {
    'padding': '0px 2px',
    border: 'solid 1px #000',
    backgroundColor: 'transparent',
    color: 'white'
  },
  over: {
    cursor: 'pointer',
    border: 'solid 1px #dcdcdc'
  }
}
